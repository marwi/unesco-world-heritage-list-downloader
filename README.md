# README #


### What is this repository for? ###

* Used to download information about world heritage properties by Unesco including photos and meta information and stores it in a XML file. Also produces two low resolution versions in 1024 and 512 pixel size of each downloaded image. 

### Information Downloaded includes ###
* Title 
* id_number
* url, category
* danger
* states
* region
* date_inscribed
* longitude
* latitude
* long-description
* short-description
* historical_description
* criteria_txt
* justification
* unique_number
* transboundary
* images
* image local relative path
* image url
* image author
* image description
* image date
* image copyright

### How do I get set up? ###

* Includes Build for Windows, Mac, Linux
* Use Processing for Development, make sure you have at least 3000 mb of ram usage allowed by Processing

### Who do I talk to? ###

* marcel@gaisterhand.de

### Examle of produced XML Structure ###


```
#!XML
<?xml version="1.0" encoding="UTF-8"?>
<unesco>
  <property>
    <title>Qhapaq Ã‘an, Andean Road System</title>
    <id>0</id>
    <id_number>1459</id_number>
    <url>http://whc.unesco.org/en/list/1459</url>
    <category>Cultural</category>
    <danger/>
    <states>Argentina,Bolivia (Plurinational State of),Chile,Colombia,Ecuador,Peru</states>
    <region>Latin America and the Caribbean</region>
    <date_inscribed>2014</date_inscribed>
    <long>-69.5916666667</long>
    <lat>-18.25</lat>
    <long-description/>
    <short-description>&lt;p&gt;This site is an extensive Inca communication, trade and defence network of roads covering 30,000&amp;nbsp;km. Constructed by the Incas over several centuries and partly based on pre-Inca infrastructure, this extraordinary network through one of the world&amp;rsquo;s most extreme geographical terrains linked the snow-capped peaks of the Andes &amp;ndash; at an altitude of more than 6,000&amp;nbsp;m &amp;ndash; to the coast, running through hot rainforests, fertile valleys and absolute deserts. It reached its maximum expansion in the 15th century, when it spread across the length and breadth of the Andes. The Qhapac &amp;Ntilde;an, Andean Road System includes 273 component sites spread over more than 6,000&amp;nbsp;km that were selected to highlight the social, political, architectural and engineering achievements of the network, along with its associated infrastructure for trade, accommodation and storage, as well as sites of religious significance.&lt;/p&gt;&#13;&lt;p&gt;&lt;span&gt;&amp;nbsp;&lt;/span&gt;&lt;/p&gt;</short-description>
    <historical_description/>
    <criteria_txt>(ii)(iii)(iv)(vi)</criteria_txt>
    <justification/>
    <unique_number>2003</unique_number>
    <transboundary>1</transboundary>
    <images>
      <image>
        <id>0</id>
        <relativePath>0/0-original.jpg</relativePath>
        <url>http://whc.unesco.org/uploads/sites/site_1459.jpg</url>
      </image>
      <image>
        <id>1</id>
        <author>Proyecto QÃ‘-Bolivia</author>
        <description>Qhapaq Ã‘an, Andean Road System</description>
        <date>12/07/2011</date>
        <copyright>copy;&amp;nbsp;Proyecto QÃ‘-Bolivia</copyright>
        <relativePath>0/1-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1459_0020.jpg</url>
      </image>
      <image>
        <id>2</id>
        <author>Proyecto QÃ‘-Bolivia</author>
        <description>Qhapaq Ã‘an, Andean Road System</description>
        <date>11/07/2011</date>
        <copyright>copy;&amp;nbsp;Proyecto QÃ‘-Bolivia</copyright>
        <relativePath>0/2-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1459_0021.jpg</url>
      </image>
      <image>
        <id>3</id>
        <author>Proyecto QÃ‘-Bolivia</author>
        <description>Qhapaq Ã‘an, Andean Road System</description>
        <date>11/07/2011</date>
        <copyright>copy;&amp;nbsp;Proyecto QÃ‘-Bolivia</copyright>
        <relativePath>0/3-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1459_0022.jpg</url>
      </image>
      <image>
        <id>4</id>
        <author>Proyecto QÃ‘-Bolivia</author>
        <description>Qhapaq Ã‘an, Andean Road System</description>
        <date>12/07/2011</date>
        <copyright>copy;&amp;nbsp;Proyecto QÃ‘-Bolivia</copyright>
        <relativePath>0/4-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1459_0023.jpg</url>
      </image>
      <image>
        <id>5</id>
        <author>Proyecto QÃ‘-Bolivia</author>
        <description>Qhapaq Ã‘an, Andean Road System</description>
        <date>10/07/2011</date>
        <copyright>copy;&amp;nbsp;Proyecto QÃ‘-Bolivia</copyright>
        <relativePath>0/5-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1459_0024.jpg</url>
      </image>
      <image>
        <id>6</id>
        <author>Proyecto QÃ‘-Bolivia</author>
        <description>Qhapaq Ã‘an, Andean Road System</description>
        <date>05/02/2011</date>
        <copyright>copy;&amp;nbsp;Proyecto QÃ‘-Bolivia</copyright>
        <relativePath>0/6-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1459_0025.jpg</url>
      </image>
      <image>
        <id>7</id>
        <author>Proyecto QÃ‘-Bolivia</author>
        <description>Qhapaq Ã‘an, Andean Road System</description>
        <date>10/07/2011</date>
        <copyright>copy;&amp;nbsp;Proyecto QÃ‘-Bolivia</copyright>
        <relativePath>0/7-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1459_0026.jpg</url>
      </image>
      <image>
        <id>8</id>
        <author>Proyecto QÃ‘-Bolivia</author>
        <description>Qhapaq Ã‘an, Andean Road System</description>
        <date>10/07/2011</date>
        <copyright>copy;&amp;nbsp;Proyecto QÃ‘-Bolivia</copyright>
        <relativePath>0/8-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1459_0027.jpg</url>
      </image>
      <image>
        <id>9</id>
        <author>Proyecto QÃ‘-Bolivia</author>
        <description>Qhapaq Ã‘an, Andean Road System</description>
        <date>10/07/2011</date>
        <copyright>copy;&amp;nbsp;Proyecto QÃ‘-Bolivia</copyright>
        <relativePath>0/9-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1459_0028.jpg</url>
      </image>
      <image>
        <id>10</id>
        <author>Proyecto QÃ‘-Bolivia</author>
        <description>Qhapaq Ã‘an, Andean Road System</description>
        <date>11/07/2011</date>
        <copyright>copy;&amp;nbsp;Proyecto QÃ‘-Bolivia</copyright>
        <relativePath>0/10-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1459_0029.jpg</url>
      </image>
      <image>
        <id>11</id>
        <author>Proyecto QÃ‘-Bolivia</author>
        <description>Qhapaq Ã‘an, Andean Road System</description>
        <date>11/07/2011</date>
        <copyright>copy;&amp;nbsp;Proyecto QÃ‘-Bolivia</copyright>
        <relativePath>0/11-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1459_0030.jpg</url>
      </image>
      <image>
        <id>12</id>
        <author>Proyecto QÃ‘-Bolivia</author>
        <description>Qhapaq Ã‘an, Andean Road System</description>
        <date>12/07/2011</date>
        <copyright>copy;&amp;nbsp;Proyecto QÃ‘-Bolivia</copyright>
        <relativePath>0/12-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1459_0031.jpg</url>
      </image>
    </images>
  </property>
  <property>
    <title>Jesuit Missions of the Guaranis: San Ignacio Mini, Santa Ana, Nuestra SeÃ±ora de Loreto and Santa Maria Mayor (Argentina), Ruins of Sao Miguel das Missoes (Brazil)</title>
    <id>1</id>
    <id_number>275</id_number>
    <url>http://whc.unesco.org/en/list/275</url>
    <category>Cultural</category>
    <danger/>
    <states>Argentina,Brazil</states>
    <region>Latin America and the Caribbean</region>
    <date_inscribed>1983</date_inscribed>
    <long>-54.26583333</long>
    <lat>-28.54333333</lat>
    <long-description>&lt;p&gt;The remains of these Jesuit missions are outstanding examples of a type of building and of an architectural ensemble which illustrate a significant period of the history of Argentina and Brazil. The ruins of Sa&amp;otilde; Miguel das Miss&amp;otilde;es in Brazil and those of San Ignacio Min&amp;iacute;, Santa Ana, Nuestra Se&amp;ntilde;ora de Loreto and Santa Mar&amp;iacute;a la Mayor in Argentina lie in the heart of a tropical forest. They are impressive remains of five Jesuit missions, built in the land of the Guaranis during the 17th and 18th centuries.&lt;/p&gt;&#13;&lt;p&gt;All these Guarani &lt;em&gt;reducciones&lt;/em&gt; (settlements) are laid out on the same model: the church, the residence of the Fathers, and the regularly spaced houses of the Indians are laid out around a large square. However, each of the &lt;em&gt;reducciones&lt;/em&gt; is characterized by a specific layout and a different state of conservation.&lt;/p&gt;&#13;&lt;p&gt;San Ignacio Min&amp;iacute;, founded in 1611, was moved on two successive occasions, settling in its present site in 1696. It incorporates important monumental remains: churches, residence of the Fathers, schools. The ruins are accessible and in a relatively good state of preservation. It is the most eminent example of a &lt;em&gt;reduccion&lt;/em&gt; preserved on Argentinean territory.&lt;/p&gt;&#13;&lt;p&gt;Santa Ana, founded in 1633 on the Sierra del Tape, was removed in 1638 to the bank of the Paran&amp;aacute; river and once more to its present site, 45&amp;nbsp;km from Posadas. The ruins of the church, which are accessible by a monumental stairway, emerge from a forest site. It resisted pillage following the expulsion of the Jesuits in 1767.&lt;/p&gt;&#13;&lt;p&gt;Nuestra Se&amp;ntilde;ora de Loreto, founded in 1610, was moved in 1631 to its present site 53&amp;nbsp;km from Posadas. The mission included a printing-press. The church and the Fathers' house were built by Brother Bressanelli, as at San Ignacio Min&amp;iacute;. The ruins of the Indian village have been partially cleared of vegetation&lt;/p&gt;&#13;&lt;p&gt;Santa Mar&amp;iacute;a la Mayor, founded in 1626, was moved to its present site in 1633. Not far from the ruins of the church, important remains of the residence of the fathers are still standing.&lt;/p&gt;&#13;&lt;p&gt;Sa&amp;otilde; Miguel, founded on the site of the Itaiaceco in 1632, was transferred first to Concepci&amp;oacute;n, and then in 1687 to its present site on the banks of the Piratini. Of the village not one building remains intact; all that are visible are the foundations of the Fathers' residence, the school, and the walls of the cemetery, along with some vestiges of Indian habitations. In a site that is periodically invaded by vegetation are found the ruins of the church attributed to Father Gian Battista Primoli, a Jesuit architect of Milanese origins, well known for his work in Buenos Aires, C&amp;oacute;rdoba and Concepci&amp;oacute;n. This Baroque church, finished in 1750, was ravaged 10 years by fire. It was restored in a rather summary manner during the years which preceded the definitive expulsion of the Jesuits in 1768.&lt;/p&gt;</long-description>
    <short-description>&lt;p&gt;The ruins of S&amp;atilde;o Miguel das Miss&amp;otilde;es in Brazil, and those of San Ignacio Min&amp;iacute;, Santa Ana, Nuestra Se&amp;ntilde;ora de Loreto and Santa Mar&amp;iacute;a la Mayor in Argentina, lie at the heart of a tropical forest. They are the impressive remains of five Jesuit missions, built in the land of the Guaranis during the 17th and 18th centuries. Each is characterized by a specific layout and a different state of conservation.&lt;/p&gt;</short-description>
    <historical_description/>
    <criteria_txt>(iv)</criteria_txt>
    <justification/>
    <unique_number>326</unique_number>
    <transboundary>1</transboundary>
    <images>
      <image>
        <id>0</id>
        <relativePath>1/0-original.jpg</relativePath>
        <url>http://whc.unesco.org/uploads/sites/site_275.jpg</url>
      </image>
      <image>
        <id>1</id>
        <author>Adam Jones</author>
        <description>Jesuit Missions of the Guaranis: San Ignacio Mini, Santa Ana, Nuestra SeÃ±ora de Loreto and Santa Maria Mayor (Argentina), Ruins of Sao Miguel das Missoes (Brazil)</description>
        <date>25/07/2009</date>
        <copyright>copy;&amp;nbsp;adamjones.freeservers.com</copyright>
        <relativePath>1/1-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0275_0001.jpg</url>
      </image>
      <image>
        <id>2</id>
        <author>M &amp; G Therin-Weise</author>
        <description>Ruins of the Jesuit reduction San Ignacio Mini, Church gate, Misiones Province, Argentina, South AmericaRuinen der Jesuitenreduktion San Ignacio Mini, Kirchentor, Misiones Provinz, Argentinien, SÃ¼damerika</description>
        <date>17/07/2011</date>
        <copyright>copy;&amp;nbsp;M &amp; G Therin-Weise</copyright>
        <relativePath>1/2-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0275_0002.jpg</url>
      </image>
      <image>
        <id>3</id>
        <author>M &amp; G Therin-Weise</author>
        <description>Jesuit Missions of the Guaranis: San Ignacio Mini, Santa Ana, Nuestra SeÃ±ora de Loreto and Santa Maria Mayor (Argentina), Ruins of Sao Miguel das Missoes (Brazil)</description>
        <copyright>copy;&amp;nbsp;M &amp; G Therin-Weise</copyright>
        <relativePath>1/3-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0275_0003.jpg</url>
      </image>
      <image>
        <id>4</id>
        <author>M &amp; G Therin-Weise</author>
        <description>Jesuit Missions of the Guaranis: San Ignacio Mini, Santa Ana, Nuestra SeÃ±ora de Loreto and Santa Maria Mayor (Argentina), Ruins of Sao Miguel das Missoes (Brazil)</description>
        <copyright>copy;&amp;nbsp;M &amp; G Therin-Weise</copyright>
        <relativePath>1/4-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0275_0004.jpg</url>
      </image>
      <image>
        <id>5</id>
        <author>M &amp; G Therin-Weise</author>
        <description>Jesuit Missions of the Guaranis: San Ignacio Mini, Santa Ana, Nuestra SeÃ±ora de Loreto and Santa Maria Mayor (Argentina), Ruins of Sao Miguel das Missoes (Brazil)</description>
        <copyright>copy;&amp;nbsp;M &amp; G Therin-Weise</copyright>
        <relativePath>1/5-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0275_0005.jpg</url>
      </image>
      <image>
        <id>6</id>
        <author>M &amp; G Therin-Weise</author>
        <description>Jesuit Missions of the Guaranis: San Ignacio Mini, Santa Ana, Nuestra SeÃ±ora de Loreto and Santa Maria Mayor (Argentina), Ruins of Sao Miguel das Missoes (Brazil)</description>
        <copyright>copy;&amp;nbsp;M &amp; G Therin-Weise</copyright>
        <relativePath>1/6-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0275_0006.jpg</url>
      </image>
      <image>
        <id>7</id>
        <author>M &amp; G Therin-Weise</author>
        <description>Jesuit Missions of the Guaranis: San Ignacio Mini, Santa Ana, Nuestra SeÃ±ora de Loreto and Santa Maria Mayor (Argentina), Ruins of Sao Miguel das Missoes (Brazil)</description>
        <copyright>copy;&amp;nbsp;M &amp; G Therin-Weise</copyright>
        <relativePath>1/7-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0275_0007.jpg</url>
      </image>
      <image>
        <id>8</id>
        <author>M &amp; G Therin-Weise</author>
        <description>Jesuit Missions of the Guaranis: San Ignacio Mini, Santa Ana, Nuestra SeÃ±ora de Loreto and Santa Maria Mayor (Argentina), Ruins of Sao Miguel das Missoes (Brazil)</description>
        <copyright>copy;&amp;nbsp;M &amp; G Therin-Weise</copyright>
        <relativePath>1/8-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0275_0008.jpg</url>
      </image>
      <image>
        <id>9</id>
        <author>Ko Hon Chiu Vincent</author>
        <description>Jesuit Missions of the Guaranis: San Ignacio Mini, Santa Ana, Nuestra SeÃ±ora de Loreto and Santa Maria Mayor (Argentina), Ruins of Sao Miguel das Missoes (Brazil)</description>
        <date>26/05/2011</date>
        <copyright>copy;&amp;nbsp;Ko Hon Chiu Vincent</copyright>
        <relativePath>1/9-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0275_0009.jpg</url>
      </image>
      <image>
        <id>10</id>
        <author>Ko Hon Chiu Vincent</author>
        <description>Jesuit Missions of the Guaranis: San Ignacio Mini, Santa Ana, Nuestra SeÃ±ora de Loreto and Santa Maria Mayor (Argentina), Ruins of Sao Miguel das Missoes (Brazil)</description>
        <date>26/05/2011</date>
        <copyright>copy;&amp;nbsp;Ko Hon Chiu Vincent</copyright>
        <relativePath>1/10-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0275_0010.jpg</url>
      </image>
      <image>
        <id>11</id>
        <author>Ko Hon Chiu Vincent</author>
        <description>Jesuit Missions of the Guaranis: San Ignacio Mini, Santa Ana, Nuestra SeÃ±ora de Loreto and Santa Maria Mayor (Argentina), Ruins of Sao Miguel das Missoes (Brazil)</description>
        <date>26/05/2011</date>
        <copyright>copy;&amp;nbsp;Ko Hon Chiu Vincent</copyright>
        <relativePath>1/11-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0275_0011.jpg</url>
      </image>
      <image>
        <id>12</id>
        <author>Ko Hon Chiu Vincent</author>
        <description>Jesuit Missions of the Guaranis: San Ignacio Mini, Santa Ana, Nuestra SeÃ±ora de Loreto and Santa Maria Mayor (Argentina), Ruins of Sao Miguel das Missoes (Brazil)</description>
        <date>26/05/2011</date>
        <copyright>copy;&amp;nbsp;Ko Hon Chiu Vincent</copyright>
        <relativePath>1/12-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0275_0013.jpg</url>
      </image>
    </images>
  </property>
  <property>
    <title>Prehistoric Pile dwellings around the Alps</title>
    <id>2</id>
    <id_number>1363</id_number>
    <url>http://whc.unesco.org/en/list/1363</url>
    <category>Cultural</category>
    <danger/>
    <states>Austria,France,Germany,Italy,Slovenia,Switzerland</states>
    <region>Europe and North America</region>
    <date_inscribed>2011</date_inscribed>
    <long>8.2075</long>
    <lat>47.2783333333</lat>
    <long-description/>
    <short-description>&lt;p&gt;This serial property of 111 small individual sites encompasses the remains of prehistoric pile-dwelling (or stilt house) settlements in and around the Alps built from around 5000 to 500 B.C. on the edges of lakes, rivers or wetlands. Excavations, only conducted in some of the sites, have yielded evidence that provides insight into life in prehistoric times during the Neolithic and Bronze Age in Alpine Europe and the way communities interacted with their environment. Fifty-six of the sites are located in Switzerland. The settlements are a unique group of exceptionally well-preserved and culturally rich archaeological sites, which constitute one of the most important sources for the study of early agrarian societies in the region.&lt;/p&gt;</short-description>
    <historical_description/>
    <criteria_txt>(iv)(v)</criteria_txt>
    <justification/>
    <unique_number>1782</unique_number>
    <transboundary>1</transboundary>
    <images>
      <image>
        <id>0</id>
        <relativePath>2/0-original.jpg</relativePath>
        <url>http://whc.unesco.org/uploads/sites/site_1363.jpg</url>
      </image>
      <image>
        <id>1</id>
        <author>P. PÃ©trequin</author>
        <description>Original piles in Lac de Chalain, rive occidentale (FR-39-02)with the reconstruction of a Neolithic dwelling in the bqckground</description>
        <copyright>copy;&amp;nbsp;P. PÃ©trequin, Centre de la Recherches Archeologique de la VallÃ©e de lâ€™Ain</copyright>
        <relativePath>2/1-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1363_0001.jpg</url>
      </image>
      <image>
        <id>2</id>
        <relativePath>2/2-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_1363_0002.jpg</url>
      </image>
    </images>
  </property>
  <property>
    <title>FertÃ¶ / Neusiedlersee Cultural Landscape</title>
    <id>3</id>
    <id_number>772</id_number>
    <url>http://whc.unesco.org/en/list/772</url>
    <category>Cultural</category>
    <danger/>
    <states>Austria,Hungary</states>
    <region>Europe and North America</region>
    <date_inscribed>2001</date_inscribed>
    <long>16.72272222</long>
    <lat>47.71927778</lat>
    <long-description>&lt;p&gt;The Fert&amp;ouml;-Neusiedler Lake and its surroundings are an outstanding example of a traditional human settlement and land use representative of a culture. The present character of the landscape is the result of millennia-old land-use forms based on stockraising and viticulture to an extent not found in other European lake areas. The historic centre of the medieval free town of Rust constitutes an outstanding example of a traditional human settlement representative of the area. The town exhibits the special building mode of a society and culture within which the lifestyles of townspeople and farmers form a united whole. The Fert&amp;ouml;-Neusiedler Lake has been the meeting place of different cultures for eight millennia, and this is graphically demonstrated by its varied landscape, the result of an evolutionary and symbiotic process of human interaction with the physical environment.&lt;/p&gt;&#13;&lt;p&gt;The lake lies between the Alps, 70&amp;nbsp;km distant, and the lowlands in the territory of two states, Austria and Hungary. The lake itself is in an advanced state of sedimentation, with extensive reed stands. It has existed for 500 years within an active water management regime. In the 19th century, canalization of Hans&amp;aacute;g shut the lake off from its freshwater marshland. Since 1912 completion of a circular dam ending at Hegyk&amp;ouml; to the south has prevented flooding.&lt;/p&gt;&#13;&lt;p&gt;Two broad periods may be discerned: from around 6000 BC until the establishment of the Hungarian state in the 11th century AD and from the 11th century until the present. The World Heritage site lies in a region that was Hungarian territory from the 10th century until the First World War. From the 7th century BC the lake shore was densely populated, initially by people of the early Iron Age Hallstatt culture and on through late prehistoric and Roman times. In the fields of almost every village around the Lake there are remains of Roman villas. The basis of the current network of towns and villages was formed in the 12th and 13th centuries, their markets flourishing from 1277 onwards, when they were relieved of many fiscal duties.&lt;/p&gt;&#13;&lt;p&gt;The mid-13th century Tatar invasion left this area unharmed, and it enjoyed uninterrupted development throughout medieval times until the Turkish conquest in the late 16th century. The economic basis throughout was the export of animals and wine. Rust in particular prospered on the wine trade. Its refortification in the early 16th century as a response to the then emerging Ottoman threat marked the beginning of a phase of construction in the area, first with fortifications and then, during the 17th-19th centuries, with the erection and adaptation of domestic buildings. The remarkable rural architecture of the villages surrounding the lake and several 18th-and 19th-century palaces add to the area's considerable cultural interest. The palace of the township of Nagycenk and the Fert&amp;ouml;d Palace are included in detached areas of the core zone outside the buffer zone.&lt;/p&gt;&#13;&lt;p&gt;Sz&amp;eacute;chenyi Palace, at the southern end of the lake, is a detached ensemble of buildings in the centre of a large park, initially built in the mid-18th century on the site of a former manor house. It acquired some of its present form and appearance around 1800. The Baroque palace garden was originated in the 17th century. In the late 18th century an English-style landscape garden was laid out.&lt;/p&gt;&#13;&lt;p&gt;Between 1769 and 1790 Josef Haydn's compositions were first heard in the Fert&amp;ouml;d Esterh&amp;aacute;zy Palace. It was the most important 18th-century palace of Hungary, built on the model of Versailles. The plan of the palace, garden and park was on geometrical lines which extended to the new village of Esterh&amp;aacute;za. There, outside the palace settlement, were public buildings, industrial premises and residential quarters. The palace itself is laid out around a square with rounded internal corners. To the south is an enormous French Baroque garden that has been changed several times, the present layout being essentially that of 1762.&lt;/p&gt;</long-description>
    <short-description>&lt;p&gt;The Fert&amp;ouml;/Neusiedler Lake area has been the meeting place of different cultures for eight millennia. This is graphically demonstrated by its varied landscape, the result of an evolutionary symbiosis between human activity and the physical environment. The remarkable rural architecture of the villages surrounding the lake and several 18th- and 19th-century palaces adds to the area&amp;rsquo;s considerable cultural interest.&lt;/p&gt;</short-description>
    <historical_description>&lt;p&gt;Two broad periods can be discerned: from c 6000 BC until the establishment of the Hungarian state in the 11th century AD and from the 11th century until the present. The nomination lies in a region that was Hungarian territory from the 10th century until World War I.&lt;/p&gt;&#13;&lt;p&gt;The landscape began to be developed from at least the 6th millennium BC. Then, early Neolithic communities lived in large permanent villages: a row of such settlements follows the southern shore of the Lake. Cultural and trading connections with neighbouring areas are characteristics of a later Neolithic phase. Distinct cultural attributes distinguish a phase at the beginning of the 4th millennium when settlements were on different sites and cattle-raising was the basis of the economy. Metallurgy was introduced around 2000 BC, and thereafter this area shared in what appears to be a general European prosperity in the 2nd millennium BC. One of its manifestations was the dispersal of amber: the Amber Route connecting the Baltic and the Adriatic passed near the Lake.&lt;/p&gt;&#13;&lt;p&gt;From the 7th century BC onwards the shore of the Lake was densely populated, initially by people of the Early Iron Age Hallstatt culture and on through late prehistoric and Roman times. In the fields of almost every village around the Lake there are remains of Roman villas. Two in Fert&amp;ouml;r&amp;aacute;kos are accompanied by a 3rd century AD Mithraic temple which is open to visitors. The Roman hegemony was ended in the late 4th century AD, however, by the first of numerous invasions, beginning a phase of continual change and bewildering replacement of one people by another until the Avar Empire in the 9th century. Hungarians occupied the Carpathian Basin and became the overlords of the Lake area around AD 900.&lt;/p&gt;&#13;&lt;p&gt;A new state and public administration system was established in the 11th century. Sopron, a place with prehistoric and Roman origins, became the seat of the bailiff and centre of the county named after it. The basis of the current network of towns and villages was formed in the 12th and 13th centuries, their markets flourishing from 1277 onwards, when they were effectively relieved of many fiscal duties. A migration of German settlers started in the 13th century and continued throughout the Middle Ages. The mid-13th century Tatar invasion left this area unharmed, and it enjoyed uninterrupted development throughout medieval times until the Turkish conquest in the late 16th century. The economic basis throughout was the export of animals and wine.&lt;/p&gt;&#13;&lt;p&gt;Rust in particular prospered on the wine trade. Its refortification in the early 16th century as a response to the then emerging Ottoman threat marked the beginning of a phase of construction in the area, first with fortifications and then, during the 17th-19th centuries, with the erection and adaptation of domestic buildings. The liberation of the peasants after 1848 and the political situation after 1867 promoted development and building activity was renewed. The most important events locally in the second half of the 19th century were the construction of railways and the completion of the water management facilities. In the 20th century, the Austro-Hungarian frontier created after World War I divided the area into two, but true isolation started only with the establishment of the Iron Curtain between the Communist world and the rest of Europe after World War II. It was at Fert&amp;ouml;r&amp;aacute;kos, "the place where the first brick was knocked out of the Berlin wall," that participants at a Pan-European Picnic tore down the barbed wire and re-opened the frontier which still crosses the Lake.&lt;/p&gt;</historical_description>
    <criteria_txt>(v)</criteria_txt>
    <justification>&lt;p&gt;&lt;em&gt;Criterion (v):&lt;/em&gt; The Fert&amp;ouml;-Neusiedler Lake has been the meeting place of different cultures for eight millennia, and this is graphically demonstrated by its varied landscape, the result of an evolutionary and symbiotic process of human interaction with the physical environment.&lt;/p&gt;</justification>
    <unique_number>913</unique_number>
    <transboundary>1</transboundary>
    <images>
      <image>
        <id>0</id>
        <relativePath>3/0-original.jpg</relativePath>
        <url>http://whc.unesco.org/uploads/sites/site_772.jpg</url>
      </image>
      <image>
        <id>1</id>
        <author>Nomination File</author>
        <description>FertÃ¶ / Neusiedlersee Cultural Landscape</description>
        <relativePath>3/1-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0772_0001.jpg</url>
      </image>
      <image>
        <id>2</id>
        <author>Ko Hon Chiu Vincent</author>
        <description>FertÃ¶ / Neusiedlersee Cultural Landscape</description>
        <date>16/05/2008</date>
        <copyright>copy;&amp;nbsp;Ko Hon Chiu Vincent</copyright>
        <relativePath>3/2-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0772_0002.jpg</url>
      </image>
      <image>
        <id>3</id>
        <author>Ko Hon Chiu Vincent</author>
        <description>FertÃ¶ / Neusiedlersee Cultural Landscape</description>
        <date>16/05/2008</date>
        <copyright>copy;&amp;nbsp;Ko Hon Chiu Vincent</copyright>
        <relativePath>3/3-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0772_0003.jpg</url>
      </image>
      <image>
        <id>4</id>
        <author>Ko Hon Chiu Vincent</author>
        <description>FertÃ¶ / Neusiedlersee Cultural Landscape</description>
        <date>16/05/2008</date>
        <copyright>copy;&amp;nbsp;Ko Hon Chiu Vincent</copyright>
        <relativePath>3/4-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0772_0005.jpg</url>
      </image>
      <image>
        <id>5</id>
        <author>Ko Hon Chiu Vincent</author>
        <description>FertÃ¶ / Neusiedlersee Cultural Landscape</description>
        <date>16/05/2008</date>
        <copyright>copy;&amp;nbsp;Ko Hon Chiu Vincent</copyright>
        <relativePath>3/5-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0772_0006.jpg</url>
      </image>
      <image>
        <id>6</id>
        <author>Ko Hon Chiu Vincent</author>
        <description>FertÃ¶ / Neusiedlersee Cultural Landscape</description>
        <date>16/05/2008</date>
        <copyright>copy;&amp;nbsp;Ko Hon Chiu Vincent</copyright>
        <relativePath>3/6-original.jpg</relativePath>
        <url>http://whc.unesco.org//uploads/sites/gallery/original/site_0772_0007.jpg</url>
      </image>
    </images>
  </property>
[...]
</unesco>
```