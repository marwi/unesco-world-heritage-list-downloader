import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.xml.*; 
import java.io.File; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class main extends PApplet {

  


Unesco unesco;
String root, current;

public void setup()
{
	background(0);
	size(600,400);
	noLoop();

	root = sketchPath("");
	root = createFolder(root+"../../data-"+month()+"-"+day()+"-"+hour()+"-"+minute()+"-"+second()+"/");

	unesco = new Unesco(this);
	unesco.rss = "http://whc.unesco.org/en/list/xml/";
	unesco.start();
}

public String remove(String input, String r)
{
	int i = input.indexOf(r);
	if(i <= -1) return input;
	String p1 = input.substring(0,i);
	String p2 = input.substring(i+r.length());
	String output = p1 + p2;
	return output;
}

// return path to currently created folder if successfull
public String createFolder(String path)
{
	File f = new File(path);
	//Path p = new Path(path);
	// overwriting
	if (f.exists()) 
	{
		println("Deleting Files");
		deleteAllFiles(f);
		f.delete();
	}

    // create dir
    if(f.mkdir() == true)
    	return path;
    else 
    {
    	println("Could not create folder at " + path);
    	return path;
    }
}

// recursive deletion of all files and directory in certain path
public void deleteAllFiles(File path)
{
	if(!path.exists())
		return;

	File[] files = path.listFiles();
	for (int i = 0; i < files.length; i++) 
	{
		File f = files[i];
		// first clear directory.
		if (f.isDirectory()) 
			deleteAllFiles(f);
		
		f.delete();
	}
}

public void draw()
{
	background(0);
	unesco.draw();


	if (Runtime.getRuntime().freeMemory() > 1000000) 
	{
		println("Free Memory: " + Runtime.getRuntime().freeMemory());
	} else 
	{
		System.gc(); 
	}
}
class Property
{
	Unesco parent;
	int id;
	ArrayList<Image> imgs;
	String title, description, shortDesc;
	String url;
	double longitude, latitude;
	String category;
	String danger, date_inscribed, criteria_txt, historical_description, id_number;
	String justification, region, states, unique_number, transboundary;

	Property(Unesco parent)
	{
		this.parent = parent;
		imgs = new ArrayList<Image>();
	}

	public void set(String _title, String _description, String shortDesc, String _url, double _longitude, double _latitude, String category)
	{
		title = trim(_title);
		description = trim(_description);
		this.shortDesc = trim(shortDesc);
		url = trim(_url);
		longitude = _longitude;
		latitude = _latitude;
		this.category = category; 
		//println("Property: " + title + " - url: " + url);
		//println(description);
	}

	public void loadImages()
	{
		String lines[] = loadStrings(url+"/gallery");
		// loop lines
		for(String s : lines)
		{
			String l = trim(s);
			if(l.length() <= 0) continue;
			// search for images in uploads folder:
			String imgRegex = "/uploads/(.*?).jpg";
			String[] imgURLs = match(l, imgRegex);
			if(imgURLs != null)
			{
				String imgURL = imgURLs[0];
				// only process if the image has a lightbox class attached
				if(l.contains("lightbox"))
				{
					// add domain to url
					imgURL = parent.domain + imgURL;
					Meta meta = new Meta();
					// get full url to lightbox from line
					String[] lightboxURLs = match(l, "/(.*?)(=[0-9]{1,})"); // start with /, then any strings until a block with a = and any number of numbers follow
					if(lightboxURLs != null)
					{
						String lightboxURL = parent.domain + lightboxURLs[0];
						//println(lightboxURL);
						meta = getMetaInformation(lightboxURL);
						//meta.printAll();
					}
					addImage(imgURL, meta);
					//println(l);
					//println(imgURL);
				}

			}
		}
	}

	public Meta getMetaInformation(String url)
	{
		Meta meta = new Meta();
		String lines[] = loadStrings(url);
		for(String l : lines)
		{
			l = l.trim();
			if(l.length() <= 0)
				continue;

			// get everything behind </strong> tag until <br /> tag
			String tag = "<\\/strong>";
			String[] s = match(l, tag+"(?:(?!(<br ?\\/>)).)*"); 
			if(s != null) 
			{
				if(s[0].length() < tag.length()) continue;
				String content = s[0].substring(tag.length());
				content = trim(content);
				if(l.contains("Description")) meta.description = content;
				else if(l.contains("Copyright")) meta.copyright = content;
				else if(l.contains("Author")) meta.author = content;
				else if(l.contains("Date")) meta.date = content;
			}
		}
		meta.id = str(imgs.size());
		return meta;
	}

	public void addImage(String url, Meta meta)
	{
		try 
		{
			Image img = new Image();
			img.set(url, meta);
			imgs.add(img);
		}
		catch(Exception e)
		{
			println("Error loading Image: " + e);
		}
	}

	public void save()
	{
		current = createFolder(root+id+"/");
		for(int i = 0; i < imgs.size(); i++)
		{
			Image img = (Image) imgs.get(i);
			String path = current;
			img.save(path, id+"/");
		}

		imgs.clear();
	}
}

/*
*	Wrapper Class for Image Information
*/
class Image
{
	String url;
	Meta meta;

	Image()
	{}

	public void set(String url, Meta meta)
	{
		this.url = url;
		this.meta = meta;
	}

	public void save(String path, String folder)
	{
		println("Current Memory usage: " + Runtime.getRuntime().totalMemory() + " of " + Runtime.getRuntime().maxMemory());
		try
		{
			String original_path = path;
			path += meta.id + "-original.jpg";
			PImage img = loadImage(url);
			unesco.recentImage = null;
			unesco.recentImage = img;
			redraw();
			meta.relativePath = trim(folder)+meta.id+"-original.jpg";
			println("Saving " + url + " to " + meta.relativePath);
			img.save(path);
			saveResized(img, original_path);
			g.removeCache(img);
		}
		catch(NullPointerException e)
		{
			println("Image does not exist: " + e);
		}
		catch(OutOfMemoryError e)
		{
			println("Out of Memory..." + e);
			println("Used Memory: " + Runtime.getRuntime().totalMemory());
		}
		catch(Exception e)
		{
			println("Unexpected Error saving Image " +  e);
		}
	}

	public void saveResized(PImage img, String path)
	{
		if(img.width > img.height)
		{
			img.resize(1024,0);
			img.save(path+meta.id+"-1024.jpg");
			img.resize(512,0);
			img.save(path+meta.id+"-512.jpg");
			
		}
		else
		{
			img.resize(0,1024);
			img.save(path+meta.id+"-1024.jpg");
			img.resize(0,512);
			img.save(path+meta.id+"-512.jpg");
			
		}
	}
}


/*
*	Wrapper Class for Meta Information
*/
class Meta
{
	String id = "0";
	String description, date, copyright, author, relativePath;

	Meta()
	{}

	public void printAll()
	{
		println("Meta ID: " + id);
		println("Meta Description: " + description);
		println("Meta Author: " + author);
		println("Meta Date: " + date);
		println("Meta Copyright: " + copyright);
	}
}
class Unesco extends Thread
{
	boolean running = false;
	String rss;
	int entries, currentEntry = 0;
	String progress = "";
	String info = "...";
	float percent = 0;
	PImage recentImage;

	// id of properties where to start the download
	int startAt = 148;

	PApplet parent;
	String domain;
	ArrayList<Property> properties;

	String xmlRoot = "<unesco></unesco>";

	Unesco(PApplet parent)
	{
		this.parent = parent;
		properties = new ArrayList<Property>();
	}

	public void start () 
	{
		super.start();
	}

	public void run()
	{
		read(rss);
		saveAsXML();
		quit();
	}

	public void quit() 
	{
		println("Thread Ending");
		interrupt();
	}

	public void draw()
	{		
		if(recentImage != null)
		{
			imageMode(CENTER);
			if(recentImage.width > width)
				recentImage.resize((int)(width * .8f),0);
			if(recentImage.height > height)
				recentImage.resize(0,(int)(height * .8f));
			image(recentImage, width/2, height/2);
		}

		noStroke();
		fill(0,200);
		rect(0,0,width,35);

		int h = 3;
		fill(20);
		rect(0,(h*2)/4, width, h);
		fill(255, 50, 100);
		rect(0, h/2,width*percent/100,h);

		fill(255);
		textAlign(CENTER, TOP);
		int d = 8;
		text(progress+" % - Downloading Entry #" + currentEntry + " of " + entries + " Entries.", 0, d, width, 100);

		fill(0,100);
		rect(0, height-60, width, 60);
		fill(255);
		textAlign(CENTER, CENTER);
		text(info, 20, height-55, width-40, 50);
	}

	public void read(String rssURL)
	{
		XML temp = parseXML(xmlRoot);

		println("reading RSS: " + rssURL);
		getDomain(rssURL);

		XMLElement rss = new XMLElement(parent, rssURL);  
		println(rss);
		XMLElement[] itemElements = rss.getChildren("row");  
		// Loop Items from RSS Feed, read data and store in Property Object
		for (int i = startAt; i < itemElements.length; i++) 
		{  
			entries = itemElements.length;
			currentEntry = i+1;
			percent = ((float)(i+1) / (float)itemElements.length)* 100;
			progress = nf(percent,0,1);
			redraw();
			//if(i > 0)
			//	break;
			XMLElement item = itemElements[i];
			// title
			String title = get("site", item);
			println(title);
			// description
			String fullDesc = get("long_description", item);
			if(fullDesc == null) fullDesc = "";
			String shortDesc = get("short_description", item);
			if(shortDesc == null) shortDesc = "";
			String historical_description = get("historical_description", item);
			if(historical_description == null) historical_description = "";
			// filter description with regular expression because by default
			// description contains url to image as well as a descriptive text
			// image url
			//String imgRegex = "http://(.*?).jpg";
			//String imgURL = match(fullDesc, imgRegex)[0];
			String imgURL = get("image_url", item);
			if(imgURL == null) imgURL = "";
			// description text
			//String descriptionRegex = "('>)(.*\\.)"; // starts with '> then get text until .
			//String description = match(fullDesc, descriptionRegex)[0];
			//description = description.substring(2,description.length());
			// url of article
			String url = get("http_url", item);
			// latitude and longitute
			String geoLat = get("latitude", item);
			if(geoLat == null) geoLat = "";
			String geoLong = get("longitude", item);
			if(geoLong == null) geoLong = "";
			String category = get("category", item);
			if(category == null) category = "";
			String danger = get("danger", item);
			if(danger == null) danger = "";
			String date_inscribed = get("date_inscribed", item);
			if(date_inscribed == null) date_inscribed = "";
			String criteria_txt = get("criteria_txt", item);
			if(criteria_txt == null) criteria_txt = "";
			String id_number = get("id_number", item);
			if(id_number == null) id_number = "";
			String justification = get("justification", item);
			if(justification == null) justification = "";
			String region = get("region", item);
			if(region == null) region = "";
			String states = get("states", item);
			if(states == null) states = "";
			String unique_number = get("unique_number", item);
			if(unique_number == null) unique_number = "";
			String transboundary = get("transboundary", item);
			if(transboundary == null) transboundary = "";

			Property p = new Property(this);
			Double longitude = Double.parseDouble(geoLong);
			Double latitude = Double.parseDouble(geoLat);
			p.set(title, fullDesc, shortDesc, url, longitude, latitude, category);
			p.id = i;
			p.id_number = id_number;
			p.danger = danger;
			p.date_inscribed = date_inscribed;
			p.criteria_txt = criteria_txt;
			p.historical_description = historical_description;
			p.justification = justification;
			p.region = region;
			p.states = states;
			p.unique_number = unique_number;
			p.transboundary = transboundary;
			info = p.title;
			p.addImage(imgURL, new Meta());
			p.loadImages();
			p.save();
			//properties.add(p);

			savePropertyToXML(p, temp, "temp");
			println(progress + "% reading done");
		}
	}

	public void saveAsXML()
	{
		XML data = parseXML(xmlRoot);
		for(Property property : properties)
		{
			savePropertyToXML(property, data, "data");
		}
	}

	private void savePropertyToXML(Property property, XML xml, String name)
	{
		XML p = xml.addChild("property");
		// write data to property element
		addChild(p, "title", property.title);
		addChild(p, "id", str(property.id));
		addChild(p, "id_number", property.id_number);
		addChild(p, "url", property.url);
		addChild(p, "category", property.category);
		addChild(p, "danger", property.danger);
		addChild(p, "states", property.states);
		addChild(p, "region", property.region);
		addChild(p, "date_inscribed", property.date_inscribed);
		addChild(p, "long", Double.toString(property.longitude));
		addChild(p, "lat", Double.toString(property.latitude));
		addChild(p, "long-description", property.description);
		addChild(p, "short-description", property.shortDesc);
		addChild(p, "historical_description", property.historical_description);
		addChild(p, "criteria_txt", property.criteria_txt);
		addChild(p, "justification", property.justification);
		addChild(p, "unique_number", property.unique_number);
		addChild(p, "transboundary", property.transboundary);

		XML imgs = p.addChild("images");
		for(Image img : property.imgs)
		{
			XML i = imgs.addChild("image");
			addChild(i, "id", img.meta.id);
			if(img.meta.author != null)
				addChild(i, "author", img.meta.author);
			if(img.meta.description != null)
				addChild(i, "description", img.meta.description);
			if(img.meta.date != null)
				addChild(i, "date", img.meta.date);
			if(img.meta.copyright != null)
				addChild(i, "copyright", img.meta.copyright);
			if(img.meta.relativePath != null)
				addChild(i, "relativePath", img.meta.relativePath);
			if(img.url != null)
				addChild(i, "url", img.url);
		}

		saveXML(xml, root + name+".xml");
	}

	private void addChild(XML to, String name, String content)
	{
		XML e = to.addChild(name);
		e.setContent(content);
	}

	private void getDomain(String url)
	{
		// get domain
		String[] domains = match(url, "http://(.*?)\\..*?/");
		if(domains != null)
		{
			domain = domains[0];
			println("domain: " + domain);
		}
	}

	// Helper Method for Parsing XML Text
	private String get(String type, XMLElement item)
	{
		XMLElement[] types = item.getChildren(type); 
		for(XMLElement t : types)
			return t.getContent();
		return "";
	}
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "main" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
