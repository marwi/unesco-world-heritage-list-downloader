class Property
{
	Unesco parent;
	int id;
	ArrayList<Image> imgs;
	String title, description, shortDesc;
	String url;
	double longitude, latitude;
	String category;
	String danger, date_inscribed, criteria_txt, historical_description, id_number;
	String justification, region, states, unique_number, transboundary;

	Property(Unesco parent)
	{
		this.parent = parent;
		imgs = new ArrayList<Image>();
	}

	void set(String _title, String _description, String shortDesc, String _url, double _longitude, double _latitude, String category)
	{
		title = trim(_title);
		description = trim(_description);
		this.shortDesc = trim(shortDesc);
		url = trim(_url);
		longitude = _longitude;
		latitude = _latitude;
		this.category = category; 
		//println("Property: " + title + " - url: " + url);
		//println(description);
	}

	void loadImages()
	{
		String lines[] = loadStrings(url+"/gallery");
		// loop lines
		for(String s : lines)
		{
			String l = trim(s);
			if(l.length() <= 0) continue;
			// search for images in uploads folder:
			String imgRegex = "/uploads/(.*?).jpg";
			String[] imgURLs = match(l, imgRegex);
			if(imgURLs != null)
			{
				String imgURL = imgURLs[0];
				// only process if the image has a lightbox class attached
				if(l.contains("lightbox"))
				{
					// add domain to url
					imgURL = parent.domain + imgURL;
					Meta meta = new Meta();
					// get full url to lightbox from line
					String[] lightboxURLs = match(l, "/(.*?)(=[0-9]{1,})"); // start with /, then any strings until a block with a = and any number of numbers follow
					if(lightboxURLs != null)
					{
						String lightboxURL = parent.domain + lightboxURLs[0];
						//println(lightboxURL);
						meta = getMetaInformation(lightboxURL);
						//meta.printAll();
					}
					addImage(imgURL, meta);
					//println(l);
					//println(imgURL);
				}

			}
		}
	}

	Meta getMetaInformation(String url)
	{
		Meta meta = new Meta();
		String lines[] = loadStrings(url);
		for(String l : lines)
		{
			l = l.trim();
			if(l.length() <= 0)
				continue;

			// get everything behind </strong> tag until <br /> tag
			String tag = "<\\/strong>";
			String[] s = match(l, tag+"(?:(?!(<br ?\\/>)).)*"); 
			if(s != null) 
			{
				if(s[0].length() < tag.length()) continue;
				String content = s[0].substring(tag.length());
				content = trim(content);
				if(l.contains("Description")) meta.description = content;
				else if(l.contains("Copyright")) meta.copyright = content;
				else if(l.contains("Author")) meta.author = content;
				else if(l.contains("Date")) meta.date = content;
			}
		}
		meta.id = str(imgs.size());
		return meta;
	}

	void addImage(String url, Meta meta)
	{
		try 
		{
			Image img = new Image();
			img.set(url, meta);
			imgs.add(img);
		}
		catch(Exception e)
		{
			println("Error loading Image: " + e);
		}
	}

	void save()
	{
		current = createFolder(root+id+"/");
		for(int i = 0; i < imgs.size(); i++)
		{
			Image img = (Image) imgs.get(i);
			String path = current;
			img.save(path, id+"/");
		}
	}
}

/*
*	Wrapper Class for Image Information
*/
class Image
{
	String url;
	Meta meta;

	Image()
	{}

	void set(String url, Meta meta)
	{
		this.url = url;
		this.meta = meta;
	}

	void save(String path, String folder)
	{
		println("Current Memory usage: " + Runtime.getRuntime().totalMemory()/mb + " of " + Runtime.getRuntime().maxMemory()/mb);
		try
		{
			String original_path = path;
			path += meta.id + "-original.jpg";
			PImage img = loadImage(url);
			unesco.recentImage = null;
			unesco.recentImage = img;
			redraw();
			meta.relativePath = trim(folder)+meta.id+"-original.jpg";
			println("Saving " + url + " to " + meta.relativePath);
			img.save(path);
			saveResized(img, original_path);
			g.removeCache(img);
		}
		catch(NullPointerException e)
		{
			println("Image does not exist: " + e);
		}
		catch(OutOfMemoryError e)
		{
			println("Out of Memory..." + e);
			println("Used Memory: " + Runtime.getRuntime().totalMemory() /mb);
		}
		catch(Exception e)
		{
			println("Unexpected Error saving Image " +  e);
		}
	}

	void saveResized(PImage img, String path)
	{
		if(img.width > img.height)
		{
			img.resize(1024,0);
			img.save(path+meta.id+"-1024.jpg");
			img.resize(512,0);
			img.save(path+meta.id+"-512.jpg");
			
		}
		else
		{
			img.resize(0,1024);
			img.save(path+meta.id+"-1024.jpg");
			img.resize(0,512);
			img.save(path+meta.id+"-512.jpg");
			
		}
	}
}


/*
*	Wrapper Class for Meta Information
*/
class Meta
{
	String id = "0";
	String description, date, copyright, author, relativePath;

	Meta()
	{}

	void printAll()
	{
		println("Meta ID: " + id);
		println("Meta Description: " + description);
		println("Meta Author: " + author);
		println("Meta Date: " + date);
		println("Meta Copyright: " + copyright);
	}
}
