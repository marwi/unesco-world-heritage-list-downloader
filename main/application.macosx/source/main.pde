import processing.xml.*;  
import java.io.File;

Unesco unesco;
String root, current;

void setup()
{
	background(0);
	size(600,400);
	noLoop();

	root = sketchPath("");
	root = createFolder(root+"../../data-"+month()+"-"+day()+"-"+hour()+"-"+minute()+"-"+second()+"/");

	unesco = new Unesco(this);
	unesco.rss = "http://whc.unesco.org/en/list/xml/";
	unesco.start();
}

String remove(String input, String r)
{
	int i = input.indexOf(r);
	if(i <= -1) return input;
	String p1 = input.substring(0,i);
	String p2 = input.substring(i+r.length());
	String output = p1 + p2;
	return output;
}

// return path to currently created folder if successfull
String createFolder(String path)
{
	File f = new File(path);
	//Path p = new Path(path);
	// overwriting
	if (f.exists()) 
	{
		println("Deleting Files");
		deleteAllFiles(f);
		f.delete();
	}

    // create dir
    if(f.mkdir() == true)
    	return path;
    else 
    {
    	println("Could not create folder at " + path);
    	return path;
    }
}

// recursive deletion of all files and directory in certain path
void deleteAllFiles(File path)
{
	if(!path.exists())
		return;

	File[] files = path.listFiles();
	for (int i = 0; i < files.length; i++) 
	{
		File f = files[i];
		// first clear directory.
		if (f.isDirectory()) 
			deleteAllFiles(f);
		
		f.delete();
	}
}

void draw()
{
	background(0);
	unesco.draw();


	if (Runtime.getRuntime().freeMemory() > 1000000) 
	{
		println("Free Memory: " + Runtime.getRuntime().freeMemory());
	} else 
	{
		System.gc(); 
	}
}
