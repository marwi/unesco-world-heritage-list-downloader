class Unesco extends Thread
{
	boolean running = false;
	String rss;
	int entries, currentEntry = 0;
	String progress = "";
	String info = "...";
	float percent = 0;
	PImage recentImage;

	// id of properties where to start the download
	int startAt = 148;

	PApplet parent;
	String domain;
	ArrayList<Property> properties;

	String xmlRoot = "<unesco></unesco>";

	Unesco(PApplet parent)
	{
		this.parent = parent;
		properties = new ArrayList<Property>();
	}

	void start () 
	{
		super.start();
	}

	void run()
	{
		read(rss);
		saveAsXML();
		quit();
	}

	void quit() 
	{
		println("Thread Ending");
		interrupt();
	}

	void draw()
	{		
		if(recentImage != null)
		{
			imageMode(CENTER);
			if(recentImage.width > width)
				recentImage.resize((int)(width * .8),0);
			if(recentImage.height > height)
				recentImage.resize(0,(int)(height * .8));
			image(recentImage, width/2, height/2);
		}

		noStroke();
		fill(0,200);
		rect(0,0,width,35);

		int h = 3;
		fill(20);
		rect(0,(h*2)/4, width, h);
		fill(255, 50, 100);
		rect(0, h/2,width*percent/100,h);

		fill(255);
		textAlign(CENTER, TOP);
		int d = 8;
		text(progress+" % - Downloading Entry #" + currentEntry + " of " + entries + " Entries.", 0, d, width, 100);

		fill(0,100);
		rect(0, height-60, width, 60);
		fill(255);
		textAlign(CENTER, CENTER);
		text(info, 20, height-55, width-40, 50);
	}

	void read(String rssURL)
	{
		XML temp = parseXML(xmlRoot);

		println("reading RSS: " + rssURL);
		getDomain(rssURL);

		XMLElement rss = new XMLElement(parent, rssURL);  
		println(rss);
		XMLElement[] itemElements = rss.getChildren("row");  
		// Loop Items from RSS Feed, read data and store in Property Object
		for (int i = startAt; i < itemElements.length; i++) 
		{  
			entries = itemElements.length;
			currentEntry = i+1;
			percent = ((float)(i+1) / (float)itemElements.length)* 100;
			progress = nf(percent,0,1);
			redraw();
			//if(i > 0)
			//	break;
			XMLElement item = itemElements[i];
			// title
			String title = get("site", item);
			println(title);
			// description
			String fullDesc = get("long_description", item);
			if(fullDesc == null) fullDesc = "";
			String shortDesc = get("short_description", item);
			if(shortDesc == null) shortDesc = "";
			String historical_description = get("historical_description", item);
			if(historical_description == null) historical_description = "";
			// filter description with regular expression because by default
			// description contains url to image as well as a descriptive text
			// image url
			//String imgRegex = "http://(.*?).jpg";
			//String imgURL = match(fullDesc, imgRegex)[0];
			String imgURL = get("image_url", item);
			if(imgURL == null) imgURL = "";
			// description text
			//String descriptionRegex = "('>)(.*\\.)"; // starts with '> then get text until .
			//String description = match(fullDesc, descriptionRegex)[0];
			//description = description.substring(2,description.length());
			// url of article
			String url = get("http_url", item);
			// latitude and longitute
			String geoLat = get("latitude", item);
			if(geoLat == null) geoLat = "";
			String geoLong = get("longitude", item);
			if(geoLong == null) geoLong = "";
			String category = get("category", item);
			if(category == null) category = "";
			String danger = get("danger", item);
			if(danger == null) danger = "";
			String date_inscribed = get("date_inscribed", item);
			if(date_inscribed == null) date_inscribed = "";
			String criteria_txt = get("criteria_txt", item);
			if(criteria_txt == null) criteria_txt = "";
			String id_number = get("id_number", item);
			if(id_number == null) id_number = "";
			String justification = get("justification", item);
			if(justification == null) justification = "";
			String region = get("region", item);
			if(region == null) region = "";
			String states = get("states", item);
			if(states == null) states = "";
			String unique_number = get("unique_number", item);
			if(unique_number == null) unique_number = "";
			String transboundary = get("transboundary", item);
			if(transboundary == null) transboundary = "";

			Property p = new Property(this);
			Double longitude = Double.parseDouble(geoLong);
			Double latitude = Double.parseDouble(geoLat);
			p.set(title, fullDesc, shortDesc, url, longitude, latitude, category);
			p.id = i;
			p.id_number = id_number;
			p.danger = danger;
			p.date_inscribed = date_inscribed;
			p.criteria_txt = criteria_txt;
			p.historical_description = historical_description;
			p.justification = justification;
			p.region = region;
			p.states = states;
			p.unique_number = unique_number;
			p.transboundary = transboundary;
			info = p.title;
			p.addImage(imgURL, new Meta());
			p.loadImages();
			p.save();
			//properties.add(p);

			savePropertyToXML(p, temp, "temp");
			println(progress + "% reading done");
		}
	}

	void saveAsXML()
	{
		XML data = parseXML(xmlRoot);
		for(Property property : properties)
		{
			savePropertyToXML(property, data, "data");
		}
	}

	private void savePropertyToXML(Property property, XML xml, String name)
	{
		XML p = xml.addChild("property");
		// write data to property element
		addChild(p, "title", property.title);
		addChild(p, "id", str(property.id));
		addChild(p, "id_number", property.id_number);
		addChild(p, "url", property.url);
		addChild(p, "category", property.category);
		addChild(p, "danger", property.danger);
		addChild(p, "states", property.states);
		addChild(p, "region", property.region);
		addChild(p, "date_inscribed", property.date_inscribed);
		addChild(p, "long", Double.toString(property.longitude));
		addChild(p, "lat", Double.toString(property.latitude));
		addChild(p, "long-description", property.description);
		addChild(p, "short-description", property.shortDesc);
		addChild(p, "historical_description", property.historical_description);
		addChild(p, "criteria_txt", property.criteria_txt);
		addChild(p, "justification", property.justification);
		addChild(p, "unique_number", property.unique_number);
		addChild(p, "transboundary", property.transboundary);

		XML imgs = p.addChild("images");
		for(Image img : property.imgs)
		{
			XML i = imgs.addChild("image");
			addChild(i, "id", img.meta.id);
			if(img.meta.author != null)
				addChild(i, "author", img.meta.author);
			if(img.meta.description != null)
				addChild(i, "description", img.meta.description);
			if(img.meta.date != null)
				addChild(i, "date", img.meta.date);
			if(img.meta.copyright != null)
				addChild(i, "copyright", img.meta.copyright);
			if(img.meta.relativePath != null)
				addChild(i, "relativePath", img.meta.relativePath);
			if(img.url != null)
				addChild(i, "url", img.url);
		}

		saveXML(xml, root + name+".xml");
	}

	private void addChild(XML to, String name, String content)
	{
		XML e = to.addChild(name);
		e.setContent(content);
	}

	private void getDomain(String url)
	{
		// get domain
		String[] domains = match(url, "http://(.*?)\\..*?/");
		if(domains != null)
		{
			domain = domains[0];
			println("domain: " + domain);
		}
	}

	// Helper Method for Parsing XML Text
	private String get(String type, XMLElement item)
	{
		XMLElement[] types = item.getChildren(type); 
		for(XMLElement t : types)
			return t.getContent();
		return "";
	}
}
